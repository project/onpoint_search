/**
 * @file
 * JS for OnPoint search submission, suggested search, and predictive autocomplete.
 */
(function ($, Drupal, drupalSettings, w) {
  'use strict';
  // D8 behaviors: https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
  Drupal.behaviors.onpoint = {
    attach: function (context, settings) {

      /* search function */
      function onPointSearchAPI() {
        // if OnPoint Search configured...
        if (!!drupalSettings.onPointSearch.onPointKey && !!drupalSettings.onPointSearch.onPointInput) {

          // bind predictive text and search submit to inputs
          var opInputs = drupalSettings.onPointSearch.onPointInput.split(',').filter(n => n.trim());

          if (opInputs.length) {
            $.each(opInputs, function (index, value) {
              var opInput = $(value.trim());
              if (opInput.length) {
                opInput.each(function () {
                  var currentInput = $(this);
                  // bind predictive autocomplete if enabled
                  if (!!drupalSettings.onPointSearch.onPointPredict) {
                    predict.bind(currentInput[0], {});
                  }

                  // submit search query
                  $('~ [type="submit"]', currentInput).click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var searchQuery = currentInput.val();

                    if (searchQuery.length > 0) {
                      window.location.href = drupalSettings.onPointSearch.onPointPath + '?opq=' + encodeURI(searchQuery);
                    }
                  });
                });
              }
            });
          }


          // add suggested search to main search input
          if (!!drupalSettings.onPointSearch.onPointPrimary && !!drupalSettings.onPointSearch.onPointSuggested) {
            // get suggested search terms, filter out empty elements
            var opSuggestions = drupalSettings.onPointSearch.onPointSuggested.split('\n').filter(n => n);

            opSuggestions = opSuggestions.map((suggestion) => '<li><a href="' + window.location.protocol + '//' + window.location.host + drupalSettings.onPointSearch.onPointPath + '?opq=' + encodeURI(suggestion) + '" tabindex="-1">' + suggestion + '</a></li>');

            var opPrimaryInputs = drupalSettings.onPointSearch.onPointPrimary.split(',').filter(n => n.trim());

            if (opPrimaryInputs.length) {
              var opFocus = [];

              $.each(opPrimaryInputs, function (index, value) {
                var opPrimaryInput = $(value.trim());

                if (opPrimaryInput.length) {
                  opPrimaryInput.each(function () {
                    var currentInput = $(this);
                    var currentID = 'focus_' + Math.random().toString(36).substr(2, 9);

                    // append markup for search suggestions
                    currentInput.after('<ul class="search-suggestions" role="listbox" tabindex="0"><li><h6>' + Drupal.t(drupalSettings.onPointSearch.onPointSuggestedTitle + ':') + '</h6></li></ul>');

                    var currentSuggestions = currentInput.siblings('.search-suggestions');

                    // append suggestions into search markup
                    currentSuggestions.append(opSuggestions);

                    // handle focus for search input and suggestions
                    opFocus[currentID] = 0;
                    $(' a', currentSuggestions).focus(function () {
                      opFocus[currentID]++;
                    });

                    // show search suggestions
                    currentInput.focus(function () {
                      currentSuggestions.not(':visible').fadeIn(200);
                      opFocus[currentID]++;
                    });

                    // handle blur for primary search input and suggestions
                    var csf = $([currentInput, $(' a', currentSuggestions)]).map(function () {
                      return this.toArray();
                    });

                    csf.blur(function () {
                      opFocus[currentID]--;

                      setTimeout(function () {
                        if (opFocus[currentID] <= 0 && !csf.is(':focus')) {
                          currentSuggestions.fadeOut(200);
                          opFocus[currentID] = 0;
                        }
                      }, 200);
                    });

                  });
                }
              });
            }

            // suggested search list keyboard navigation
            $('.search-suggestions').keydown(function (e) {
              if (e.which === 38 || e.which === 40) {
                e.preventDefault(); // prevent page scrolling while focused in suggested search items

                var cs = $(':focus');
                var ns = e.which === 38 ? cs.parent().prev('li:not(:first-of-type)').find('a') : cs.parent().next('li').find('a');

                if (ns.length === 0) {
                  ns = e.which === 38 ? $('li:last a', $(this)) : $('li:nth-child(2) a', $(this));
                }

                ns.focus();
              } else if (e.which === 27) {
                $(this).prevAll('input').focus();
              }
            });
          }
        }
      }

      $(window).on('load', function () {
        onPointSearchAPI();
      });
    }
  };
})(jQuery, Drupal, drupalSettings, window);
