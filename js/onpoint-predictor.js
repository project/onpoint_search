// modified from: https://github.com/mocanuga/inlinePredictor
(function ($, drupalSettings, w) {
  'use strict';

  const debounce = (callback, time) => {
    let interval;
    return (...args) => {
      clearTimeout(interval);
      interval = setTimeout(() => {
        interval = null;
        callback(...args);
      }, time);
    };
  };

  var predict = function () {
    var defaults = {
      sentenceMode: false,
      stopRule: false,
      service: '',
      prediction: null
    };
    var elems = Object.create(null);
    var opts = Object.create(null);
    var active = null;
    var last;
    var predictions;

    function extend(destination, source) {
      var property;

      for (property in source) {
        if (source[property] && source[property].constructor && source[property].constructor === Object) {
          destination[property] = destination[property] || {};
          extend(destination[property], source[property]);
        } else {
          destination[property] = source[property];
        }
      }

      return destination;
    }

    function select(el, start, end) {
      if (el.setSelectionRange) {
        el.setSelectionRange(start, end);
      } else if (el.createTextRange) {
        var sel = el.createTextRange();
        sel.collapse(true);
        sel.moveStart('character', start);
        sel.moveEnd('character', end);
        sel.select();
      } else if (typeof el.selectionStart != 'undefined') {
        el.selectionStart = start;
        el.selectionEnd = end;
      }

      el.focus();
    }

    function handle(t, p, w, v) {
      var l = v.length;
      var s = (function (v, l) {
        if (!l || w.lastIndexOf(v, 0) !== 0) {
          for (var i = 0, j = p.length; i < j; i++) {
            if (p[i].toLowerCase().lastIndexOf(v, 0) === 0) {
              last = p[i].toLowerCase();

              break;
            }
          }
        }

        return !!last ? last.toLowerCase().replace(v, '') : '';
      }(v, last));
      var i = (opts.sentenceMode ? w.join(' ') : w);

      t.value = i + s;
      l = t.value.length;
      select(t, l - s.length, l);

      if (typeof opts.prediction === 'function') {
        opts.prediction.call(null, t, t.value, i, s);
      }
    }

    function defer(t, w, v = '') {
      function processPredict(data) {
        if (data.suggestions.length && Array.isArray(data.suggestions)) {
          data.suggestions.sort((a, b) => (a.weight > b.weight) ? 1 : (a.weight === b.weight) ? ((a.term > b.term) ? 1 : -1) : -1);

          var r = data.suggestions.map(x => x.term);

          predictions = r.slice();
          handle(t, r, w, v);
        }
      }

      var termsUrl = 'https://search.onpointsuite.ca/api/search-suggest/?search_engine=' + drupalSettings.onPointSearch.onPointKey + '&url_filter=&url_exclude_filter=&q=' + v + '&callback=loadSuggestions';

      $.ajax({
        dataType: 'jsonp',
        url: encodeURI(termsUrl),
        success: processPredict
      });
    }

    var kd = debounce(function(e) {
      var t = e.target;
      var start = t.selectionStart;
      var tv = t.value;
      var v = tv.substr(0, start);

      if (e.keyCode == 8 || e.keyCode == 46) {
        e.target.value = v;

        $(t).siblings('.search-suggestions').fadeOut(200);

        return;
      }

      // if search suggestions present and visible on tab, navigate to list
      if (e.keyCode == 9 && $(t).siblings('.search-suggestions').length && $(t).siblings('.search-suggestions:visible').length) {
        e.preventDefault();
        e.stopPropagation();

        $(t).siblings('.search-suggestions').find('li:nth-child(2) a').focus();

        return;
      }

      if (!!opts.stopRule && !!v.match(opts.stopRule)) {
        e.preventDefault();
        e.stopPropagation();
      }
    }, 100);

    var kp = debounce(function(e) {
      // skip modifier keys
      if (e.which !== 0 && e.keyCode !== 0 && (e.ctrlKey || e.metaKey || e.altKey)) {
        return;
      }

      e.stopPropagation();
      e.preventDefault();

      var t = e.target;
      var tv = t.value;
      var v;
      var l;

      $(t).siblings('.search-suggestions').fadeOut(200);

      if (opts.sentenceMode) {
        var w = tv.split(" ");
        var v = w.slice(-1)[0].toLowerCase();
      } else {
        v = tv;
      }

      l = v.length;

      if (!l) {
        return true;
      }

      if (!!opts.stopRule && !!v.match(opts.stopRule)) {
        t.value = tv;

        return false;
      }

      if (!last || !!last && last.lastIndexOf(v, 0) !== 0) {
        defer(t, (opts.sentenceMode ? w : tv), v);
        last = null;
      } else {
        handle(t, predictions, (opts.sentenceMode ? w : tv), v);
      }
    }, 250);

    function activate(e) {
      if (!!elems[e.target.id] && !e.target.isSameNode(active || document.createElement('a'))) {
        active = e.target;
        opts = elems[e.target.id].opts;
      }
    }

    function deactivate(e) {
      active = null;
      opts = Object.create(null);
    }

    return {
      bind: function (element, options) {
        element.addEventListener('focus', activate, true);
        element.addEventListener('blur', deactivate, true);
        element.addEventListener('keypress', kp, true);
        element.addEventListener('keydown', kd, true);
        elems[element.getAttribute('id')] = {};
        elems[element.getAttribute('id')].opts = extend(defaults, options);
      },
      unbind: function (element) {
        element.removeEventListener('focus', activate, true);
        element.removeEventListener('blur', deactivate, true);
        element.removeEventListener('keypress', kp, true);
        element.removeEventListener('keydown', kd, true);
        delete elems[element.getAttribute('id')];
      }
    }
  }

  w['predict'] = new predict();
}(jQuery, drupalSettings, window));
