(function (w, d, s, o, f, js, fjs) {
  w['OnPointSearch'] = o; w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
  js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
  js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
}(window, document, 'script', 'onpointsearch', 'https://search.onpointsuite.ca/embed/v2/' + drupalSettings.onPointSearch.onPointKey + '/'));
onpointsearch('init', drupalSettings.onPointSearch.onPointKey);
