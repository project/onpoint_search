<?php

namespace Drupal\onpoint_search_d8\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for OnPoint Search embed code.
 */
class OnPointController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function embed() {
    // Default settings.
    $config = \Drupal::config('onpoint_search_d8.settings');

    // OnPoint Search API key.
    $onpoint_key = $config->get('onpoint_key');

    if ($onpoint_key !== '') {
      $element = [
        '#onpoint_serp' => [
          '#type' => 'onpoint',
          '#markup' => '<div id="onpointsearch-root"></div>',
        ],
        '#theme' => 'onpoint_serp',
      ];

      $element['#onpoint_serp']['#cache']['tags'][] = 'onpoint_search_d8_lib';
      $element['#onpoint_serp']['#attached']['library'][] = 'onpoint_search_d8/onpoint_serp';

      return $element;
    }
    else {
      \Drupal::logger('onpoint_search_d8')->error('OnPoint API key empty.');
    }

    return;
  }

}
