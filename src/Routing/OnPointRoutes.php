<?php

namespace Drupal\onpoint_search_d8\Routing;

use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class OnPointRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    // Default settings.
    $config = \Drupal::config('onpoint_search_d8.settings');

    // OnPoint Search page title.
    $onpoint_title = $config->get('onpoint_title') ?: 'Search Results';

    // OnPoint Search page path.
    $onpoint_path = $config->get('onpoint_path') ?: '/search-results';

    $routes = [];

    // Declares a single route under the name 'onpoint_search_d8.embed'.
    $routes['onpoint_search_d8.embed'] = new Route(
      // Path to attach this route to:
      $onpoint_path,
      // Route defaults:
      [
        '_controller' => '\Drupal\onpoint_search_d8\Controller\OnPointController::embed',
        '_title' => $onpoint_title
      ],
      // Route requirements:
      [
        '_permission'  => 'access content',
      ]
    );

    // Returns an array of Route objects.
    return $routes;
  }

}
