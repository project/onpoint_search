<?php

namespace Drupal\onpoint_search_d8\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\Cache;

/**
 * Configure OnPoint Search settings for this site.
 */
class OnPointSettingsForm extends ConfigFormBase {
    /** @var string Config settings */
  const SETTINGS = 'onpoint_search_d8.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'onpoint_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // OnPoint results page path validation
    global $base_url;
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $onpoint_path = $form_state->getValue('onpoint_path');

    $path_exists = $onpoint_path ? \Drupal::service('path.alias_storage')->aliasExists($onpoint_path, $langcode, $base_url) : FALSE;

    if (!empty($onpoint_path) && $path_exists) {
      $form_state->setErrorByName('onpoint_path', $this->t('This path is already in use. Please choose a unique path that is not already being used on your site.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['onpoint_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('onpoint_key'),
      '#description' => $this->t('Enter your OnPoint Search API key.'),
      '#required' => TRUE,
      '#size' => 36,
      '#maxlength' => 36,
      '#pattern' => '^([0-9A-Fa-f]{8})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{12})$',
    ];

    $form['onpoint_input'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Input'),
      '#default_value' => $config->get('onpoint_input'),
      '#description' => $this->t('Enter the identifier for search input(s). If more than one, separate values with a comma. (E.g. \'.search-input, .search-input2\')'),
      '#maxlength' => 60,
    ];

    $form['onpoint_predict'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable predictive autocomplete?'),
      '#default_value' => $config->get('onpoint_predict'),
      '#description' => $this->t('Enables predictive autcomplete for search input fields. Autocomplete suggestions are returned from the OnPoint API.'),
    ];

    $form['onpoint_primary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Input'),
      '#default_value' => $config->get('onpoint_primary'),
      '#description' => $this->t('Enter the identifier for primary search input(s) that are to show configured \'Suggested Search Terms\'. If more than one, separate values with a comma. (E.g. \'#search-1, #search-2\')'),
      '#maxlength' => 60,
    ];

    $form['onpoint_suggested_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suggested Search Title'),
      '#default_value' => $config->get('onpoint_suggested_title'),
      '#description' => $this->t('Enter the title you wish to display for suggested search terms. Default title is \'Popular Searches\'.'),
    ];

    $form['onpoint_suggested'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Suggested Search Terms'),
      '#default_value' => $config->get('onpoint_suggested'),
      '#description' => $this->t('Enter suggested search terms you wish to display. Enter each term on a new line.'),
    ];

    $form['onpoint_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Results Page Title'),
      '#default_value' => $config->get('onpoint_title'),
      '#description' => $this->t('Enter the page title for your OnPoint Search results page. Default title is \'Search Results\'.'),
      '#maxlength' => 60,
    ];

    $form['onpoint_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Results Page Path'),
      '#default_value' => $config->get('onpoint_path'),
      '#description' => $this->t('Enter the desired path for the OnPoint Search results page. This must be a unique path that is not already being used on your site. Default path is \'/search-results\'.'),
      '#maxlength' => 60,
    ];

    $form['onpoint_css'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom CSS'),
      '#default_value' => $config->get('onpoint_css'),
      '#description' => $this->t('Enter any custom CSS for the OnPoint Search results page here.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get form values
    $onpoint_path = $form_state->getValue('onpoint_path');
    $onpoint_path = (!empty($onpoint_path) && substr($onpoint_path, 0, 1) !== '/') ? '/' . $onpoint_path : $onpoint_path;
    $onpoint_css = $form_state->getValue('onpoint_css');
    $onpoint_css_min = Xss::filter($onpoint_css);

    // Perform some basic minification on the CSS.
    if (!empty($onpoint_css)) {
      // Remove comments
      $onpoint_css_min = preg_replace('/\/\*((?!\*\/).)*\*\//', '', $onpoint_css);

      // Collapse whitespace.
      $onpoint_css_min = preg_replace('/\s{2,}/', ' ', $onpoint_css_min);

      // Remove whitespace where not needed.
      $onpoint_css_min = preg_replace('/\s*([:;{}])\s*/', '$1', $onpoint_css_min);

      // Remove final semicolon.
      $onpoint_css_min = preg_replace('/;}/', '}', $onpoint_css_min);

      // Enable GZip encoding.
      ob_start('ob_gzhandler');
    }

    // Write CSS to file.
    $css_file_uri = file_unmanaged_save_data($onpoint_css_min, 'public://onpoint_search_d8--custom-styles.min.css', FILE_EXISTS_REPLACE);

    // Get path to CSS file.
    $css_path = parse_url(file_create_url($css_file_uri))['path'];

    // Retrieve the configuration
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting
      ->set('onpoint_key', $form_state->getValue('onpoint_key'))
      ->set('onpoint_input', $form_state->getValue('onpoint_input'))
      ->set('onpoint_predict', $form_state->getValue('onpoint_predict'))
      ->set('onpoint_primary', $form_state->getValue('onpoint_primary'))
      ->set('onpoint_suggested_title', $form_state->getValue('onpoint_suggested_title'))
      ->set('onpoint_suggested', $form_state->getValue('onpoint_suggested'))
      ->set('onpoint_title', $form_state->getValue('onpoint_title'))
      ->set('onpoint_path', $onpoint_path)
      ->set('onpoint_css', $form_state->getValue('onpoint_css'))
      ->set('onpoint_css_file_path', $css_path)
      ->save();

    if ($onpoint_css !== '') {
      // Display confirmation that file has been created.
      $messenger = \Drupal::messenger();
      $messenger->addMessage(t('Custom CSS saved to the server, file URL is "%url"',
          ['%url' => file_create_url($css_file_uri)])
      );
    }

    // Invalidate the cache.
    Cache::invalidateTags(['onpoint_search_d8_lib']);

    parent::submitForm($form, $form_state);
  }

}
