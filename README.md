OnPoint Search
===========

OnPoint Search integration for Drupal 8.x.

Instructions
------------

Unpack in the *modules* directory of your Drupal installation and enable in `/admin/modules`.

Then, visit `/admin/config/search/onpoint` and enter your OnPoint Search API Key.
